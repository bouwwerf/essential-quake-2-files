==========================================================================
 LEVEL INFORMATION 
==========================================================================
TITLE:			Neutral Grounds
FILENAME:		rqmctf1
AUTHOR:			Alan "Requiem" Scott
DATE:			31 July 2004
GAME:			Quake II
TYPE:			Capture the Flage
EMAIL:			rqm@planetquake.com
HOMEPAGE:		www.planetquake.com/rqm


==========================================================================
 LEVEL DESCRIPTION
==========================================================================
Neutral Grounds is my first, and probably last, CTF map for Quake2. 
Additionally, it is the largest map I have ever constructed; weighing in 
with over 8,000 brushes, 612 lights, and 24 spawnpoints.

Unlike other Quake2 CTF maps, RQMCTF1 is purposefully asymmetrical with 
respect to the red and blue bases. The red base is more "open" but the 
player is surrounded by lava, whereas the blue base is tighter, more compact, 
but players can utilize water for quick escapes or covert avenues to return
a captured flag. This map is also designed for use with grapple hook 
enabled. In most rooms, a lift or occassional ladder is the only means 
between to separate, vertical levels. This is by design. I wanted to 
encourage players to utilize the hook to advance vertically around the map.


SINGLE PLAYER:		1 Player Start
CO-OPERATIVE:		N/A
CAPTURE THE FLAG:	24 Player Respawn Points
SUGGESTED PLAYERS:	10-30

DIFFICULTY SETTINGS:	N/A
NEW TEXTURES:		No
NEW SOUNDS:		Yes.  One modified id Sound.
CD TRACK #		N/A


==========================================================================
 CONSTRUCTION
==========================================================================
MAP BASE:		New Map from scratch
PREFABS:		None
EDITOR:			QERadiant (Build 147)
KNOWN BUGS:		None
COMPILE MACHINE:	Pentium4 3.0GHz, 1GB RAM, ATI Radeon 9500Pro
COMPILE TIME:		1 hr 05 min

BETA TESTERS:		LeRay, kold, Stah, Skeletor, Panzer13 


==========================================================================
 OTHER LEVELS
==========================================================================
"MINECORE" for DOOM (3-Level Single Player Unit)
"Battleground" for Quake2 (Large Deathmatch Map)
"Underground" for Quake2 (Mid-size Deathmatch Map)
"Para Bellum" for Quake2 (Large Deathmatch Map)
"Return to the Edge" for Quake2 (Remake of Q2DM1)
"Duel Twilight" for Quake3 (Mid-size Deathmatch Map)
"A Cold Wind" for Quake3 (Small Deathmatch Map)

==========================================================================
 COPYRIGHT / PERMISSIONS
==========================================================================
Authors MAY NOT use this level as a base to build additional levels.
 
You MUST NOT distribute this level UNLESS you INCLUDE THIS FILE WITH NO MODIFICATIONS.
If you don't co-operate, then DON'T DISTRIBUTE IT IN ANY FORM.

This LEVEL may be distributed ONLY over the Internet and/or BBS systems.
You are NOT authorized to put this LEVEL on any CD or distribute it in
any way without my written permission.


