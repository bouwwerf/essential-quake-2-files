---------------------------------------------------------------
***************************************************************
Map  Name:  "Mayhem in the Middle"
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics_ctf3.zip /marics_ctf3.bsp /marics_ctf3.txt/
Contact:         maric@backshooters.com.com
   
   
      

Version:7-26-01




***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Thnx to J who lets me test every map on him =) oh yeah and my nephew "Mox"
  and my pals in clan Goth clan.
* The textures came from the "Fesh Team" GRDM map series


*GAMEPLAY INFO*

 Single Player :  the Galds. but they hate space =)
 Deathmatch :  well, actually no
 CTF : Yeah baby!
 Cooperative :  nope
 Difficulty Settings: nope
 Base System:  P3 1gig 
 Player load : I am thinking 6-14 is about right
 

*BUILD INFO*

 Build Time:   12 hours
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  23 secs.
 QVIS3 :                  123 secs.
 QRAD3 :                  229 secs.
 Known Bugs: nope 

*INSTALLATION*

       if you got this zip file intact just aim this puppy at your Quake2
               folder and let go.
 
 
*NOTES*
       I first made this basic map design for UT CTF but have since gotten really sick of UT.
      I think it translates well to Q2, it is one action packed map...


 OH YEAH please let me know what you think good or bad, i'm not overly 
  sensitive either just go easy with the harsh language, remember we live in a "society".. 



*COPYRIGHT/PERMISSIONS*

"marics_ctf3 & "Mayhem in the Midlle"  is Copyright (c) 2000 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  

You may distribute this map on the internet, on a network or other electronic
medium where it can be obtained for FREE, i strongly urge you to pass along this 
text file
so that folks can see the time i put into typing it. That said just don't put it on any 
Cd
for distribution without first informing me at s-shaffer@home.com

Please don't use this map as a base for building other maps likewise : 
You may NOT convert this .bsp file into a .map file or reverse engineer it in any 
way.

If you get this far, you know, i really don't care what you do with the map as long 
as you
 have fun with it and don't claim it as your own if your not me of course. NOT that 
i think
 that it is good enough for others to want to claim anyhow.



Thanx : Maric



