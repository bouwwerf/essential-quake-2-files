==============================================================================
Title                   : Idcbase2 - (v1.0)
Filename                : Idcbase2.bsp, Q2Ibase2.zip
Author                  : Bruce Oberleitner (i.e. Demolition Man)
Release Date            : 12/27/98
Game                    : Quake 2
Email Address           : bober@micron.net
Web Site                : http://www.planetquake.com/fragtown

Description             : This is the second in a series of Military Bases that 
                          I built for the Idaho Deathsquad Clan. I hope you like it.  

Install Notes           : Here's what you need to do to install this package for
                          Quake 2.  First, let's assume that quake2 is installed in
                          the c:\quake2 directory.  You will need to build the following
                          directories.

                          c:\quake2\baseq2\demos
                          c:\quake2\baseq2\maps

                          Copy the Idcbase2.dm2 file into the demos directory.  
                          Copy Idcbase2.BSP and Idcbase2.map into the maps directory.  
                          
Running Map and Demos   : To run the demo type "MAP IDCBASE2.DM2" at the quake 2 
                          console.  To load the map type "MAP IDCBASE2" at the quake 2
                          console. 
Additional Credits to   
Playtesting team.         Steve Dahlin (i.e. Snake Plissken)
                          Jim Grossel (i.e. Spiff)
                          Kevin Rank (i.e. Rifter)
                          Peter Dahlin (i.e. Blade)
                         
                            
and
                          Eraser Bots
                                - These things simply kick ass!
                          id Software
                                - Once again, thanks for a great game!
                          Blues News.
                                - Wonderful source of quake information!
                         

Special Note           : If you like this level, please send me an email
                         and let me know what you think.  I will build 
                         more if I get some encouragement.  Also, if you
                         record a demo on this level, please send me a
                         copy.  I love to see carnage!!!


WebSites by Author

Fragtown Homepage        http://www.planetquake.com/fragtown/fragtown.html
Idaho DeathSquad Clan    http://netnow.micron.net/~bober/IDC.html
DoomLovers Page          http://netnow.micron.net/~bober/Doomlvr.htm

======================================================================

* Play Information *

Single Player           : No
Cooperative 2-8 Player  : No
Quakematch 2-16 Player  : YES (4 - 8 players recommended)
New Sounds              : No
New Graphics            : YES
New Music               : No
Demos Replaced          : Yes. Demolition Man battles the evil bots.
                                       

* Construction *

Base                    : Level from Scratch
Editor(s) used          : Worldcraft 
Known Bugs              : please report any to bober@micron.net


* Copyright / Permissions *

Authors MAY NOT use this level as a base to build additional levels, 
concatenations, or otherwise.

You MAY distribute this BSP, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file
intact.

===========================================================================
Other Items by Author:
Either available now or coming soon.

Quake 2
Q2fgtwn1.zip             - 1st of the Fragtown series.
Q2fgtwn2.zip             - 2nd of the Fragtown series.
Q2fgtwn3.zip             - 3rd of the Fragtown series.
Q2fgtwn4.zip             - 4th of the Fragtown series.
Q2fgtwca.zip             - Fragtown explores the world of CTF!

Q2IBase1.zip             - Idaho deathsquad military base level.
Q2IBase3.zip             - Idaho deathsquad military base level.
Q2IBase3.zip             - Idaho deathsquad military base level.

Quake 1

Fragtwn1.zip             - 1st of the Fragtown series.
Fragtwn2.zip             - 2nd of the Fragtown series.
Fragtwn3.zip             - 3rd of the Fragtown series.
Fragtwn4.zip             - 4th of the Fragtown series.
Fragtwn5.zip             - 5th of the Fragtown series
Fragtwn6.zip             - 6th of the Fragtown series
Fragtwn7.zip             - 7th of the Fragtown series
Fragtwca.zip             - Fragtown explores the world of CTF!
Fragtwcb.zip             - Same level as FragCTFa but for deathmatch
Fragtw16.zip             - 16 player Fragtown level
Fragsnds.zip             - Sounds for Fragtown!

IdcBase.zip              - Idaho deathsquad military base level.
IdcBase3.zip             - Idaho deathsquad military base level.

Fragskin.zip             - New skins for robots and deathmatch players!

Duke3d

EscapeLA.zip             - Large city levels come to Duke3d!
EscapeNY.zip             - Large city levels come to Duke3d!
EscapeSM.zip             - Small city level.

Doom2

Bpodm001.zip            - Attack on Outpost 7
Bpodm002.zip            - The Villa
Bpodm003.zip            - The Nuke processing plant
Bpodm004.zip            - Run for your life!
BpoDm005.zip            - Down by the Docks.
BpoDm006.zip            - Battlefield Chicago
BpoDm007.zip            - Threate of Blood.

