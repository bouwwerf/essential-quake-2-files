released: 4/28/1998
-------------------------------------------------------------------
 Title                   : D-Lite
 Filename                : dlite.bsp
 Author                  : headshot [Dennis Kaltwasser] 
 Email Address           : headshot@planetquake.com
 			   dennis.m.kaltwasser@germanistik.uni-giessen.de
 Description             : Quake2 Deathmatch Map for up to 8 players
			   			   			                             
 Additional Credits to   : Tim Willits for inspiration.
			   Tim Wright for the incredibly cool Arghrad.

			   
-----------------------------------------------------------------


* Play Information *

 Settings                : weapon stay for 5 and more might be a good idea.
 Deathmatch 2-8          : 3-6 ffa recommended.
 Deathmatch 9-100        : no.


* Construction *

 Build Time      	 : three weeks

 Editor(s) used  	 : Worldcraft
 Other Programs used	 : enhaced compile utilities by Geoffrey DeWan, Arghrad3.
 Known Bugs      	 : none.


* Other Info *
 
 none 

* Distribution *

 Authors MAY NOT use this level as a base to build 
 additional levels.

 This BSP may be distributed ONLY over the Internet 
 and/or BBS systems. You are NOT authorized to put 
 this BSP on any CD or distribute it in any way without 
 my permission. 

 this is to be freely distributed...if you payed money
 for it then you more than likely got dicked over.

