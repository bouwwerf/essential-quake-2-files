May 04, 1999
------====================================------
        Q2CTF02 =Establish Colonisation=
------====================================------

================================================================


Title                  : =Establish Colonisation=
Filename(s)            : =q2ctf02.bsp=
Min.Sys.req.           : =p2 + "3D-Graphics-Card=".
Author                 : =NecRo= & =CoolJoe=
Realname(s)            : =Mike= & =Arjan=
Email Address          : =starwarez@fibre.a2000.nl= & =cooljoe@nemesis.coal.nl=
ICQ #                  : =NecRo 20408741 & Cooljoe 27592135=
Description            : =CTF level for Quake2=
Additional Credits to  : =id Software=
                        

================================================================


----INSTALLATION----


= Copy q2ctf02.bsp into Quake2\baseq2\maps\... =

= Then goto START, RUN...Quake2\quake2.exe +game ctf +map q2ctf02 =
= Or type in console "map q2ctf02" =

---------------Play Information---------------


-Level Name(s)   : =q2ctf02 (=Establish Colonisation=)=
-Single	         : =No=
-Deathmatch 4-10 : =No=
-Duel            : =No=
-Coop            : =No=
-CTF             : =Yes!= 


" It's an excellent CTF map for officials wars or matches. 
It Should be played 5-5 because of the size of the map, 
its now really that big tough, but because of the fast 
gameplay you should play it 5-5 for the "REAL" action! 
We've put a bunker at each base for some defence.
There's one "Quad" in it for some real serious buttkicking!
Goodluck and have fun with it. "


---------------Construction---------------


-q2ctf02.bsp

Build Time      : = One Week =
BSP file size	: = 2.3 mb =
Compile time    : = About 7 hours =
Machine Used    : = P2 350, 128 MB SDRAM =
*QBSP3		: = +/- 30 seconds =
*QVIS3 -level 4	: = 5 hours elapsed =
*QRAD3 -extra	: = 2 hours elapsed =        


-----------------------------------------


Editor(s) used  : =Qoole 2.50 & WorldCraft=


Known Bugs      : = Some Texture Bugs =                 


---------------Legal Stuff---------------

=You may NOT include this map in ANY sort
of compilation without permission.=


Hope U like The map and thnx for reading this.

Peace,

=NecRo= & =Cooljoe=
 
----------------- End ------------------