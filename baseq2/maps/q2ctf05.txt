------====================================------
        Q2CTF05+(b) =he Final Fronteer=
------====================================------

================================================================


Title                  : The Final Fronteer
Filename(s)            : q2ctf05.bsp | q2ctf05b.bsp
Sys.req.               : Any PC with a "3D-Graphics-Card".
Author                 : Lord Darkness
Realname(s)            : Johan Geuze
Email Address          : lord_darkness@nut5.com
ICQ #                  : 3820942
Description            : CTF level for Quake2
Additional Credits to  : id Software
                        

================================================================


----INSTALLATION----


 Copy q2ctf05.bsp into Quake2\baseq2\maps\... 
 Copy \env\* quake2\baseq2\env\

 Then goto START, RUN...Quake2\quake2.exe +game ctf +map q2ctf05
 Or type in console "map q2ctf05" 
-------------------------------------------------------------------
 Copy q2ctf05b.bsp into Quake2\baseq2\maps\... 
 Copy \env\* quake2\baseq2\env\

 Then goto START, RUN...Quake2\quake2.exe +game ctf +map q2ctf05b
 Or type in console "map q2ctf05b" 

---------------Play Information---------------


-Level Name(s)   : q2ctf05(b) (The Final Fronteer)
-Single	         : No
-Deathmatch 4-10 : No
-Duel            : No
-Coop            : No
-CTF             : Yes! 





---------------Construction---------------


-q2ctf05.bsp

Build Time      :  One Week 
BSP file size	:  1.3 mb 
Machine Used    :  P2 350, 128 MB SDRAM 
*QBSP3		:  +/- 120 seconds 
*QVIS3 -level 4	:  758 seconds
*QRAD3 -extra	:  738 seconds        
-----------------------------------------


Editor(s) used  : WorldCraft 1.6


Known Bugs  5    : The elevator kicks u out of the map =)
Known Bugs  5b   : None.

---------------Legal Stuff---------------

=You may NOT include this map in ANY sort
of compilation without permission.=


Hope U like The map and thnx for reading this.

Peace,

Lord Darkness
 
----------------- End ------------------