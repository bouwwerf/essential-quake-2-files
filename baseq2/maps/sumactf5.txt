================================================================
Title                   : Pornkings CTF 1 - Subworlds
Filename                : pkctf1.bsp (renamed to sumactf5 for war-pack purpose)
Start Date              : 14-july-98
Finish Date             : 16-july-98
Map Author              : Johan (Lord Darkness) Geuze
Email Address           : PKCTF@ferion666.demon.nl
Homepage                : http://pornkings.gamepoint.nl
Description             : Cool CTF map in q2ctf1 style.
Requirements            : Quake2, CTF 1.2 for Quake2.
================================================================
* Play Information *
Single Player           : No
Cooperative             : No
Deathmatch              : No
CTF                     : Yes 
* Construction *
Base                    : Scratch
Editors used            : WorldCraft 1.6
Known Bugs              : none.
* COPYRIGHT NOTICES and PERMISSION *
This level is copyright 1998 by Johan Geuze.  Permission to use, 
copy and distribute unedited copies of this file -- [starctf1.bsp] -- 
is hereby granted, provided that no fee is charged for the use or 
availability of this file (other than the normal connection costs for 
on-line services, if applicable). The above copyright notice and this 
permission notice must be left intact in all copies of this file.
Commercial distribution of this file, in whole or in part, requires 
prior agreement with the author. Commercial distribution includes 
any means by which the user is required to pay either for the support 
(e.g. book, newsletter or CD-ROM) or for the file itself.  Unauthorized 
commercial distribution is prohibited.
If you wish to commercially distribute this file, note the following:
(1)Permission is granted by the author for the one-time commercial
distribution of this file if all of the following conditions are met:
(a)The author is notified by email prior to the file's inclusion 
on any commercial release;
(b)The author is provided with one (1) "contributor's copy" of
the finished product upon it's release.  Said "contributor's 
copy" is to be sent to the author using the shipping address 
specified by the author in his/her reply to the publisher's 
initial email notification, and is to be provided at no cost 
to the author;
(c)This file (including this copyright notice) is not to be 
modified in any way, and shall be distributed as the author 
intended;
(d)The author retains all relevant and proper copyrights to this 
work both prior to and subsequent to the commercial release of 
this file.
(2)By requesting a shipping address from the author, the publisher
signifies acceptance of, and agrees to abide by, these conditions.
By returning a valid shipping address to the publisher, the author
thereby grants the publisher one-time usage of the author's 
copyrighted work for commercial purposes.
Thank you for your interest in this work.