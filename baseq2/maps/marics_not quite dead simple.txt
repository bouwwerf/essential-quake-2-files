---------------------------------------------------------------
***************************************************************
Map  Name:  "Not quite Dead Simple"
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics_ds.zip /marics_ds.bsp /marics_ds.txt/env folder/and loads of new textures
Contact:         s-shaffer@home.com
   
   
      

Version:3-30-00
***************************************************************
---------------------------------------------------------------


Additional Credit*
* ID for my favorite electronic pastime
* the folks at PureDM http://www.quakeheads.com/~puredm/ for the reviews and contest
* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Thnx to J and LowRayder, Cyberghost and Mox for letting me test this on them
* There are loads of new textures in this map that come from several sources :
*  The folks that created the amazing "Dawn of Darkness" mod, and all the beatiful texures and sounds
       **GO HERE** http://www.dawnofdarkness.com/index2.html 
       and get this total conversion it is just the best Q2 conversion bar none !!!!
       Please read the notes /legal texts in the texture/dod folder
* The sounds for the most part were sent to me from T-Bond, a great guy
* Gotta say "huge thanx" to these fine folks that have made their hard work
       freely available for us lowly mappers to use.
* The awsome env is called "night1" and i wish i knew where it came from

*** You can see the WOMF web page at members.home.net/womfclan ***

*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  Celeron 333-not oc'd /128 RAM /Voodoo2 Blackmajic
 Player load : its up to you !!!

*BUILD INFO*

 Build Time:   12 hours
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  12 secs.
 QVIS3 :                  1397 secs.
 QRAD3 :                  689 secs.
 Known Bugs:  Are 800 r_speeds bugs ?

*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let go.
 
 
*NOTES*
   The case is made in this map that great textures can make an ok map better (opinion Maric)
    WOW !! the Dawn of Darkness TC is great and the textures are amazing go get it at :
    http://www.dawnofdarkness.com/index2.html
    that is where all but a few of the textures came from (used with permission btw)
    Thanx go to Vaxuser. 
 Ok this map is made for a PureDM mapping contest requiring that maps be "based" upon
    the Doom map "Dead Simple". That is why you might recognize the layout, with my own
    personal touches. The big ? is did i represent the map in a decent way ? without just
    making a ripoff copy hmmmmmm... big box inner and outer courtyards. Argh how do you make
    that your own. What i did was use great textures (hopefully in a good way) and loads of
    Q2 style curves. The scale is off a bit due to the draw distance limitations and the
    ever oppressive "r_speeds" . As it stands the map has speeds upwards of 900 in four of the
    inner courtyard corners (sigh). In general though the map was very playable on my p2 400
    with 6 Eraser bots or 6 3zb2 bots. Now i get to see how it does in the contest, fingers crossed.
 



*COPYRIGHT/PERMISSIONS*

"Not quite Dead Simple"  is Copyright (c) 2000 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  

**Please read the "legal" doc. included in the Dawn of Darkness (dod) texture folder

You may distribute this map on the internet, on a network or other electronic
medium where it can be obtained for FREE, i strongly urge you to pass along this 
text file
so that folks can see the time i put into typing it. That said just don't put it on any 
Cd
for distribution without first informing me at s-shaffer@home.com

Please don't use this map as a base for building other maps likewise : 
You may NOT convert this .bsp file into a .map file or reverse engineer it in any 
way.

If you get this far, you know, i really don't care what you do with the map as long 
as you
 have fun with it and don't claim it as your own if your not me of course. NOT that 
i think
 that it is good enough for others to want to claim anyhow.



Thanx : Maric



