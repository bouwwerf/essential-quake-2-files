---------------------------------------------------------------
***************************************************************
Map  Name:  "Watch Your Back !"
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics17.zip /marics17.bsp /marics17.txt/env folder/and loads of new textures
Contact:         s-shaffer@home.com
   
   
      

Version:3-01-00
***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Thnx to J who lets me test every map on him =) oh yeah and my nephew "Mox"
  and my pals in clan Goth clan 
* Huge thanx go to these guys whos map "JumpBeach" and freely shared info about how to make the 
   jump pads work and made their use for me possible : Monsto Brukes, MrHyde, Scampie :
   http://tarot.telefragged.com/ryo i can't thank them enough.
* There are loads of new textures in this map that come from several sources :
   1 - Sean Johnson @ http://graphtallica.ingava.com/ email him @ salty@ingava.com
   2 - The authors of the "Stand" textures (sorry i lost the link)
   3 - Jackboot - Kevin 'Rorshach' Johnstone, Rorshach@quakefiles.com http://www.quakefiles.com/rorshach
   4 - the telport texture was obtained from a "free" online texture site
* The sounds for the most part also came from various and sundry q2 sites, offered freely for use
* Gotta say "huge thanx" to these fine folks that have made their hard work
       freely available for us lowly mappers to use.
* The awsome env is called "stars" and i wish i knew where it came from

*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  Celeron 333-not oc'd /128 RAM /Voodoo2 Blackmajic
 Player load : ARGH-COUGH-BLEHHH !!! (i hate this catagory)
               this map plays well for 1 on 1 and : 
               i think 2-6 ffa seems to be about right
               it boils down to personal opinion dosn't it ?
 * i am no good at makin bot routes, the Erasers i use don't like the jump pads anyway
    but if you (read whoever) can make and send me a good one i'd be indebted =) * 

*BUILD INFO*

 Build Time:   14 hours
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  8 secs.
 QVIS3 :                  243 secs.
 QRAD3 :                  280 secs.
 Known Bugs:   Most likely,  a "hom" for sure , man those are hard to get rid of 

*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let go.
 
 
*NOTES*
   The case is made in this map that great textures can make an ok map better (opinion Maric)
    Ok this one turned out much better then i hoped, it is brimming with curves and long sight
    lines with many firing solutions and yet the speeds stay well within acceptible limits.
    I could only find two spots that were above 600 the rest seem to avg. around 300-400.
    All the goodies are there minus the bfg and pa and bh, i did leave the health and ammo
    a little on the slim side so shoot well and move alot.
 
 
 OH YEAH please let me know what you think good or bad, i'm not overly 
  sensitive either just go easy with the harsh language, remember we live in a "society".
 

    A few are at one of my clans WoD server at 24.113.104.178 You will need to install 
     the WoD mod to benefit the most in there. Say "hey" to all the LOF peops you see there.
    Give a "hey" to all the [GoTH] folks you see also. 
 Please drop by and look for either Maric or Havok 


*COPYRIGHT/PERMISSIONS*

"Marics16 & "Watch You Back !"  is Copyright (c) 2000 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  

You may distribute this map on the internet, on a network or other electronic
medium where it can be obtained for FREE, i strongly urge you to pass along this 
text file
so that folks can see the time i put into typing it. That said just don't put it on any 
Cd
for distribution without first informing me at s-shaffer@home.com

Please don't use this map as a base for building other maps likewise : 
You may NOT convert this .bsp file into a .map file or reverse engineer it in any 
way.

If you get this far, you know, i really don't care what you do with the map as long 
as you
 have fun with it and don't claim it as your own if your not me of course. NOT that 
i think
 that it is good enough for others to want to claim anyhow.



Thanx : Maric

Oh Yeah [GoTH] &  [LOF] rock!!!!!!!!!


