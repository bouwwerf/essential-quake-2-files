---------------------------------------------------------------
***************************************************************
Map  Name:  "Deep Blue"
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics38.zip /marics38.bsp /marics38.txt/env folder/and loads of new textures
Contact:         s-shaffer@home.com
   
   
      

Version:4-15-01
***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Thnx to J who lets me test every map on him =) oh yeah and my nephew "Mox"
  and my pals in clan Goth clan 
* Huge thanx go to these guys whos map "JumpBeach" and freely shared info about how to make the 
   jump pads work and made their use for me possible : Monsto Brukes, MrHyde, Scampie :
   http://tarot.telefragged.com/ryo i can't thank them enough.
* Almost every texture in this map came from http://planetquake.com/hfx/ THANKS EVIL
* The sounds for the most part also came from various and sundry q2 sites, offered freely for use
* Gotta say "huge thanx" to these fine folks that have made their hard work
       freely available for us lowly mappers to use.
* The awsome env is called deepsea1 and came from http://www.telefragged.com/soup/index2.html Thanks to darc
* My awsome beta testers Sven, Musahi, Mox, BadDog and J

*** 


*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  Celeron 333-not oc'd /128 RAM /Voodoo2 Blackmajic
 Player load : 1 v 1 will likely be best, but 4-6 ffa is not unreasonable.
 

*BUILD INFO*

 Build Time:   20
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  9 secs.
 QVIS3 :                  1057 secs.
 QRAD3 :                  938 secs.
 Known Bugs:              most liklely 

*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let go.
 
 
*NOTES*
  This is a shot at something new in atmoshere. I would like to know what players think
   about it.
      

    

*COPYRIGHT/PERMISSIONS*

"Marics38 Deep Blue"  is Copyright (c) 2001 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  

**Please read the "legal" doc. included in the Dawn of Darkness (dod) texture folder

You may distribute this map on the internet, on a network or other electronic
medium where it can be obtained for FREE, i strongly urge you to pass along this 
text file
so that folks can see the time i put into typing it. That said just don't put it on any 
Cd
for distribution without first informing me at s-shaffer@home.com

Please don't use this map as a base for building other maps likewise : 
You may NOT convert this .bsp file into a .map file or reverse engineer it in any 
way.

If you get this far, you know, i really don't care what you do with the map as long 
as you
 have fun with it and don't claim it as your own if your not me of course. NOT that 
i think
 that it is good enough for others to want to claim anyhow.



Thanx : Maric




