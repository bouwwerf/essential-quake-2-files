---------------------------------------------------------------
***************************************************************
Map  Name:  "The Valley of Shadow..."
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics_ctf8.zip /marics_ctf8.bsp /marics_ctf8.txt/
Contact:         maric@backshooters.com.com
   
   
      

Version:8-28-02




***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Most textures use are credited to http://planetquake.com/hfx
* Nem's Mega 3d Terrain Generator v2.2.0  http://countermap.counter-strike.net/Nemesis
* Arghrad for lighting http://www.planetquake.com/arghrad/ 


*GAMEPLAY INFO*

 Single Player : 
 Deathmatch :  well, actually no
 CTF : Yeah baby!
 Cooperative :  nope
 Difficulty Settings: nope
 Base System:  P3 1gig 
 Player load : I am thinking 6-14 is about right
 

*BUILD INFO*

 Build Time:   12 hours
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  14 secs.
 QVIS3 :                  1248 secs.
 QRAD3 :                  684 secs.
 Known Bugs: don't think  

*INSTALLATION*

       if you got this zip file intact just aim this puppy at your Quake2/baseq2
               folder and let go.
 
 
*NOTES*
   Another bog-standard two base CTF deal.
 
 



*COPYRIGHT/PERMISSIONS*

"marics_ctf8 & "The Valley of the Shadow..."  is Copyright (c) 2002 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  

Blah blah blah




Thanks : Maric



