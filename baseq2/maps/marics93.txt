---------------------------------------------------------------
***************************************************************
Map  Name:  "Guns, Etc..." marics91/m91a
   DM level for Quake 2 Awakening2 mod
   by Shon Shaffer "Maric"

Files:   marics93.zip /marics93.bsp/m93a.bsp /marics93.txt/env folder/ new textures & sounds
Contact:         maric@planeytquake.com maric@backshooters.com
   
   
      

Version 8-16-04
***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.

* I made the nasty textures... sosueme.


 
*** 


*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  P3 1 gig Verto Geforce2 64 meg DDR
 Player load : lots and lots, this is not a dueling map by any stretch

*BUILD INFO*

 Build Time:    12+ hours 
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all... oh, and Arghrad and Nems Mega 3d
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  4 seconds
 QVIS3 :                  2 minutes
 ArghRAD :                Full Bright BABY!
 Known Bugs:              errrrmm we'll see? 

*notes 
      
      
                       My take on the cartoons seen on Q2 world TV. 
                       


*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let 'er rip.
  

      

    

*COPYRIGHT/PERMISSIONS*

"marics93 & Guns, Etc..."  is Copyright (c) 2004 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  


*Legal notes ; PHFFFFTTTT!


Thanks : Maric




