---------------------------------------------------------------
***************************************************************
Map  Name:  "Crates in the Mist" marics62, m62a
   DM level for Quake 2
   by Shon Shaffer "Maric"

Files:   marics62.zip /marics62.bsp, m62a.bsp /marics62.txt
Contact:         maric@backshooters or s-shaffer@comcast.net
   
   
      

Version:04-25-02
***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* The Shader Lab for great crate textures http://shaderlab.com/beta/?p=textures/urban
* The Mapcenter for great crate textures http://mapcenter.digitalarenas.com
* I even used one [HFX] texture in this, can't seem to make a map without using his
   wonderful artwork in it somewhere http://planetquake.com/hfx
* The great env was created by aXon at http://axon.drunkencoon.com
 
*** 


*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  P3 1 gig Verto Geforce2 64 meg DDR
 Player load : lots and lots, this is not a dueling map by any stretch

*BUILD INFO*

 Build Time:   16 hours
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all.
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  21 secs
 QVIS3 :                  3 hours
 QRAD3 :                  45 minutes
 Known Bugs:              strange brush anomolies, explained in detail below... 

*notes 
       There are two versions included here. m62a is for play with the Awakening mod and the other marics62 is
        for pretty much everything else. 
       I did my very best to create a Cargo-ship (crates and all) that is semi-realistic and yet playable. I have
        never once been on a ship and have not done any research about their shape and design. So I have 
        obviously taken great liberties with the concept. Yeah I know there are no sleeping quarters and no mess but
        I saw now gameplay reasons to try and include details like that.
       There are very odd brush anomolies on the lower outside hull of the ship, they are in the "kill-zone"
        but you may notice them WHEN you fall to your death. I tried like crazy to clean them up but alas, I had no luck.
        My guess is that this is a limitation in Tread or Q2, either way it is no matter, cuz the "bugs" are never in
        view unless you drop down and die... 
     
       The weapons as has been my habit lately are "over the top" (for the Awaken version)
        and that is just the way we like things on "Backshooters -Awakening" 
        our servers are at 207.114.0.70
        our web site   www.backshooters.com  


*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let 'er rip.
  

      

    

*COPYRIGHT/PERMISSIONS*

"marics62, m62a & Crates in the Mist"  is Copyright (c) 2002 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  


You may distribute this map on the internet, on a network or other electronic
medium where it can be obtained for FREE, i strongly urge you to pass along this 
text file
so that folks can see the time i put into typing it. That said just don't put it on any 
Cd
for distribution without first informing me at s-shaffer@comcast.net

Please don't use this map as a base for building other maps likewise : 
You may NOT convert this .bsp file into a .map file or reverse engineer it in any 
way.



Thanks : Maric




