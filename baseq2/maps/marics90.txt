---------------------------------------------------------------
***************************************************************
Map  Name:  "Granite Womb" marics90/m90a
   DM level for Quake 2 Awakening2 mod
   by Shon Shaffer "Maric"

Files:   marics90.zip /marics90.bsp/m90a.bsp /marics90.txt/env folder/and few new textures
Contact:         maric@planeytquake.com maric@backshooters.com
   
   
      

Version 5-23-04
***************************************************************
---------------------------------------------------------------


Additional Credit*

* I have to give the bulk of the credit to the makers of TREAD, its an 
           absolutely great program due to its accessibility and ease of use.
* ID Software, of course, for Quake 2.
* RUST- for the plethora of info provided :  http://www.gamedesign.net 
* Nem's Mega 3d Terrain Generator v2.2.0  http://countermap.counter-strike.net/Nemesis
* Arghrad for lighting http://www.planetquake.com/arghrad/ 
* The rock textures used by permission, were obtained from http://www.quake3bits.co.uk/
* The castle-ish textures came from www.planetunreal.com/undulation
* Ogro for the brown ground texture

 
*** 


*GAMEPLAY INFO*

 Single Player :  nah
 Deathmatch :  well, thats what i made it for
 Cooperative :  nope, unless you gang up on some lpb in dm.
 Difficulty Settings: nope
 Base System:  P3 1 gig Verto Geforce2 64 meg DDR
 Player load : lots and lots, this is not a dueling map by any stretch

*BUILD INFO*

 Build Time:    20+ hours 
 Utilities Used:   *Tread 
     *more Tread 
     *and even more Tread, it has it all... oh, and Arghrad and Nems Mega 3d
     


 
 Build Machine(s):  same as above
 Final Compile Machine:         "    "      "
 QBSP3 :                  8 seconds
 QVIS3 :                  6 minutes
 ArghRAD :                5 minutes
 Known Bugs:              errrrmm we'll see? 

*notes 
      
      
                       Big cave, long fall, loads of weapons...


*INSTALLATION*

       if you got this zip file intact just aim this puppy at your baseq2
               folder and let 'er rip.
  

      

    

*COPYRIGHT/PERMISSIONS*

"marics90 & Granite Womb"  is Copyright (c) 2004 by Shon M. Shaffer
Quake 2 is Copyright (c) 1997 by ID Software Inc.  All rights reserved.
The Quake 2 logo and likenesses are trademarks of ID Software Inc.
All artwork including 3D models, textures, and skins is the exclusive
property of ID software.  


*Legal notes ; PHFFFFTTTT!


Thanks : Maric




