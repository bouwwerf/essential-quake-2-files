: C A V E :

Author: 	Jeff M. Garstecki, also known as Stecki,
Email:		stecki@frag.com or stecki@playstation.sony.com
Web:		http://www.frag.com/deconstruct
Host:		http://www.frag.com


: What Cave.cin is :

This is a cinematic with sound.  Heck, it might be the first user made .cin file for quake2 (not a demo).  Anyways, it's basically a teaser ad for a TC.  I will explain how to make a .cin file below.

this was meant as a test for my upcoming single player mission pack called HOSTILE (more info at site).  Of course this isn't gonna be out anytime in the near future, but I wanted to see if I could make a .cin file, and then get a map to play one at certain times to help explain storypoints.  and well, it all worked out just dandy.  so now i'll have some cinematics (as a seperate download of course, cuz these get huge) for you all...

: How to see it :

place cave.cin into "quake2/baseq2/video" and boot up quake2.  get the prompt down (~ key) and enter "map cave.cin" and the file should play.  or at DOS enter "quake2 +map cave.cin" and it'll load up that way.

: How to make your own :

this is a boring, time consuming process, but it's cool when done...
render out your animation as individual image files in whatever animation/render/3-d/2-d package you have.  most software packages do that, but if not, there are some programs that will extract an animation frame by frame, look around on the web (i don't know any offhand, and since my machine renders individual frames, i don't care to much about it). render/draw at 320x240 (320x180 for letterbox, but the final size should be 320x240) 

so now you gotta a bunch of pix.  for the duration of the movie, they all have to share the same 8bit pallette.  so you have to convert each frame to one pallete, although each .cin file can have a unique palette.  I use debabilizer, cause i can batch all my images, and then it sorts through, looking for a palette that is best for the entire cinematic.  some renderers will output each frame to a specific palette, which can help cut this step out (infini-d on the mac does it i think, don't know what else).  
So once all the frames share the same palette, you have to make sure they're numbered correctly, starting at frame *000.pcx.  you can tell qdata to skip to frame x.pcx though...

after that, you put them all in a folder named the same as the the images themselves. for example:

cave000.pcx - cave200.pcx are my cinematic images.  I put these in a folder named "cave."  I have my qdata.exe in "quake2/source/bin_nt/" and I place the folder named "cave" in "quake2/source/video/"  you might have to create the folder named "video."  

now you have to create the script, or .qdt file that qdata needs to interpret all the frames and it's corresponding .wav file (which is named the same as the frames, ex: cave.wav).  in the qdt file, put the following:

$video	(filename - no ext.)	#digitframes	startframe# (opt)

example:

$video	cave	3

that was it for me.  the 3 represents a frame count of upto 999 frames.  if i put a 4 there, it would expect 1000-9999 frames.  you never enter in the total # of frames.  qdata figures that out.

save this file as a *.qdt file, then drag and drop onto your qdata.exe icon.  it should convert everything, assuming it's all set okay.


: S O U N D :

the soundtrack is a seperate wave file.  it is sampled at 22050 (i used 16-bit stereo).  anything else caused mine to crash during compile.  your wav file needs to be almost the same length as the animation, otherwise, it'll crash or give M_allocate errors like crazy.  It must last longer than the cinematic, or else qdata hangs on the frame where the sound stopped.  if it's longer, i think it clips the wav file off, leaving you ok.  i tried a wav file 2x as long as the cin, but it crapped out... I'm not 100% sure, but I'm guessing the framerate at about 12-15.  cave.cin is a 200 frame animation, and I used a 15 second wav file for it.  I didn't really experiment with it though (these numbers give a 13.3 frame rate) so your results may be different.  once you get your soundfile sweet, save it as the same name as your animation list, and in the same folder.  qdata does the rest.

that's basically it outta what i've found.  I'm not sure how many of you will venture into this, instead of a demo.  these files are big and take a while to convert.  I hope this is pretty clear too.  anyone who wants to write a simpler version outta this, please do.


: D I S C L A I M E R :

you may distribute these files freely.  The animation cave.cin is copyright 1998 jeff m. garstecki.  I don't really know what you could do with it except watch it...
