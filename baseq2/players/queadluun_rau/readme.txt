Queadluun Rau
Quake 2 Plugin Player Model
by Dan Bickell
danbickell@loop.com


This PPM is a Queadluun Rau female Zentraedi (Meltrandi) battle suit, from Macross
(Yes, "Macross", the original Japanese TV series, not "Robotech" the cheap piece of 
crap Americanized version).  

Just unzip all files to quake2\baseq2\players\queadluun_rau\ directory (directory MUST
be spelled correctly for it to work, and for other people to see your model correctly).

Make sure there are no stray files in the quake2\baseq2\players\ directory- there should
be no files, only subdirectories in that directory, or PPMs may not show up in the game.

Sounds- no sounds included, just copy female sounds (or Crackwhore, if you like those 
better) into the queadluun_rau directory if you want female sounds.

VWEP- vwep is supported, but only with multiple copies of the weapon model (which is
blank, since the weapon is built into the suit arms), so there will be no diaper effect
on VWEP games.

Skins- 3 skins, one standard scheme from the original TV series, and Max and Milia versions
from Macross the Movie: Do You Remember Love?.  CTF skins (just copies of Max and Milia skins,
which are blue and red) are also included.

Tools used:

Lightwave 5.5 for model and animation,
Quake2 Modeler 0.90 for mesh and .md2 conversion,
NST for mesh refinement and skin creation (3d)
Paintshop Pro and Photoshop 4 for skin creation (2d)

Build time- 3 weeks (while learning Lightwave)

Special thanks to Lee Perry of ION Storm, this guy is the coolest.  He spent quite a bit of
time in emails and IRC chats teaching me how to use Lightwave, and I doubt I would have ever 
learned this stuff without him.  Thanks a bunch Lee.

