---------------------------------------------
  #######    #######    #######
 ###        ###        ###
 #######    #######    #######
 ###   ##   ###   ##   ###   ##
 ###   ##   ###   ##   ###   ##
  ######     ######     ######    SKINS

============================================= 

NAME          : 6foxx.pcx, 6foxx_i.pcx
		6foxx2.pcx, 6foxx2_i.pcx
		6foxx3.pcx, 6foxx3_i.pcx
		6foxx4.pcx, 6foxx4_i.pcx
DESCRIPTION   : My first Female Q2 skin.. Dedicated to 
		Orion for bugging me until I did one :)
CREATED       : 18th February 97
VERSION       : QUAKE II

To use this skin, unzip the .PCX's into
your Quake2/Baseq2/players/FEMALE directory,
Then change your skin in the Multiplayer/Player
Setup menu.

Have fun!

Roger Bacon [666]
http://www.quake2.com/skins
rogerbacon@mindless.com

============================================= 
Thanks to id Software for the best damn 
games there is!
Also greets to my fellow skinners Rorshach,
Slaine, Skinforge, Shatter, and Partycrasher!
