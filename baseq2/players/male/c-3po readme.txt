Filename                : c-3po.zip (includes c-3po.pcx, c-3po_i.pcx)

Author                  : Oscar Hidalgo (D.J.Stage One)Miami,Fl.
Email Address           : stage1@mediaone.net
Description             : A new Quake2 Male skin
                                               
* Play Information *

Game                    :Quake2 , requires Registered quake2 to use...

===================================================
* Construction *

Programs used           : Paint Shop Pro

This skin is my 3-rd. and he looks great!!!                                    
        
===========================
Instructions

Install into Quake2/Baseq2/players/male directory and away u go. Select Multiplayer
and then go into the player setup, Select the male model and move along the skins until
you see my c-3po skin...


Enjoy the skin, thank you for using it, and stay alive!!

==================================================


* Copyright *

Authors may NOT use these skins as a base to build additional
skins. And it must not be sold, without permission from the author.

You MAY distribute this ZIP, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file 
intact.

In other words, don't copy other peoples "IDEAS" & "DON'T BREAK THE RULES!!!" }:)

                                                          (STAGE1@GI)