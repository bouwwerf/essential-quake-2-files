This is my very first Quake2 player skin, so I decided to start where I started
back on the first night that Qtest1 was released when I made my very first Quake
skin (undisputed at the very first custom skin ever) of Boba Fett.

So here he is.  Enjoy.

Stick both files in your quake2\baseq2\players\male\ directory.

Feel free to use, adopt for your clan, modify, etc.  Just don't sell it, and give me
credit.

Dan Bickell

danbickell@loop.com