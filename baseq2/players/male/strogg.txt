Just unpack the female skins into the \QUAKE2\BASEQ2\PLAYERS\FEMALE
and the male skins into \QUAKE2\BASEQ2\PLAYERS\MALE Directory.
Now you can choose the skin from the Multiplayer/Player Menu.

If you want to use them or offer them, please give me credits.
My homepage is at: http://www.netcologne.de/~nc-schobefr/