---------------------------------------------------------------------------------------
QUAKE2 SKIN 	"Heatseeker" 	       			                        3-1-99
---------------------------------------------------------------------------------------

Skin:		Heatseeker HS 

Author:		Martin 'Cable' Kool (The Netherlands)

Release date:	3-1-99.

E-Mail:		cable@3dpalette.com

URL(s):		http://cable.3dpalette.com
                htp://fragland.net/cable
                http://home.worldonline.nl/~kolossos
		
Files:		readme.txt, heatseeker.pcx, heatseeker_i.pcx	

Instructions:	Unzip all these files to your quake2/baseq2/players/male
                directory. And start havin' fun!!!

---------------------------------------------------------------------------------------

Important: 	Everyone is free to use this skin the Quake II multiplayer games. Don't
                use it for clan fights/battles/tournaments.  

Thanks:		Any similarity to persons living or dead is purely coincidental.
                I used Moonchilds Mohawk skin for inspiration and it were the skinner
                interviews at rorshachs place (http://www.planetquake.com/rorshach) which
                made me pick up my brain and make a skin.  
		Thanks to id Software, Inc., for creating the three best games in
		the world (Quake, Quake II & Q3A ; I have faith). And Adobe for Photoshop 5, the
		software I used to create this skin and all the other artwork (damn guys
                fix the kerning). Oh, and also Nathan Lucas for creating the OpenGL MD2 Viewer. 

DISCLAIMER:	Don't modify this skin or use it at a base skin. Don't use this skin
		for your clan. If this skin screws up anything,
		don't blame it on me. Remember that you are the one who downloaded it!

Notes:		My latest skin. As has been awhile since I made a skin. I believe diamond was
                my last skin. It feels good to skin again, expect more soon.

---------------------------------------------------------------------------------------

PS:		Please notify me if this skin is reviewed or added to a gallery or
		archive. Want to use this skin for your clan? Well, you can't! 

---------------------------------------------------------------------------------------
