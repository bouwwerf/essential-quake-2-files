================================================================
Title                   : Alita Sounds (for Evil Bastard's Alita Player Model)

November 12, 1998.

Filename                : alitasounds.zip
Author                  : dolomite of Dteam, http://www.captured.com/dteam/
Email Address           : dolomite@planetquake.com
Description             : 
                          
 Credits: 		Miscellaneous Anime sites and sounds.  Some are original,
			But most are just grabbed from da net. =)
                          
                          Tools used to create/edit the sound files.
                          Cool Edit Pro, a Sony F-V410 mic.
                          
================================================================

* Construction *

Base                    : Anime

Editor(s) used          : Cool Edit Pro
Known Bugs              : You might get addicted to Alita
Build Time              : 1 night for base sounds and a few hours mixing.

Comments                : This is it... hope you enjoy it!

****
=)