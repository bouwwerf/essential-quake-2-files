

-=+ DAWN OF DARKNESS TOTAL CONVERSION FOR QUAKE 2 +=-
-- NOTES AND INSTRUCTIONS --



CONTENTS
========

I. INTRO
II. PLAYING DAWN OF DARKNESS
	- MOVEMENT
	- COMBAT
	- INVENTORY, CONVERSATION, INTERACTION
	- CUSTOM CONSOLE COMMANDS
III. WEAPONS & COMBAT
	- WEAPONS
	- SWORDSMANSHIP
	- THE COMBAT SYSTEM
IV. THE ENVIRONMENT
	- ENEMIES
	- NPCS & LOCATIONS
V. NOTES
	- SYSTEM REQUIREMENTS
	- FAQ
	- TEAM
	- CONTACT INFO

=====
INTRO
=====

Dawn Of Darkness takes place in a long vanished world of mighty forgotten empires and magic. It is the tale of a once revered Lucitanian warlord, Roarke The Merciless, caught between his obligations to his family, his empire and his honor. 


========================
PLAYING DAWN OF DARKNESS
========================

You are Roarke the Merciless, imprisoned in the captured Manawyd monastary by an army from the Cheitan Empire who have attacked the village you have lived in.  Roarke is an elite fighter, if perhaps a little out of practice since his days as one of Lucitan's greatest warlords. You begin playing armed with only the dagger Roarke had hidden in his boot, you must escape and find Sylmaril. How you do this is up to you.  The game is played with basically the most of the same controls as Quake 2, except for the new commands and items bindings we've added.



MOVEMENT:
---------

The the movement keys for forward, back, jump, etc., are the same as for Quake 2, you can set them to whatever you like in config.cfg 

COMBAT:
-------

Primary attack = Use 1st MOUSE BUTTON to attack with selected weapon. If using Dagger or Sword, you have to move your mouse to some direction while holding down the button.
Secondary attack = Use Q, E-keys or NUMPAD 7, 9-keys to execute a secondary attack.
Dagger 		= Press 1
Bow 			= Press 2
CrossBow 		= Press 3
Ancestral Sword	= Press 4
Triple Flintlock 	= Press 5
Blunderbuss 	= Press 6
Dragon Balls 	= Press 7
Disarm 		= Press 0
Next weapon		= / (slash)
Previous weapon	= ' (What the hell do you call this?)



INVENTORY, CONVERSATION & INTERACTION:
--------------------------------------

Toggle inventory	= Use TAB or I to turn inventory on and off
Browse inventory	= Use ARROW KEYS or [ & ] (What do you call these?)
Browse conversation	= Use ARROW KEYS or [ & ] (What do you call these?)
Open/Activate/Select	= Use SPACE to open doors/chests, activate interactions and select things in the inventory and in conversations
Use item		= Press ENTER to use the active item.
Use curing items	= Press H to automatically use various curing items you may be carrying
Use shovel		= Press 8 to activate shovel and 1st MOUSE BUTTON to dig
Use torch		= Press 9 or T to activate torch. Make sure you have a free hand.



CUSTOM CONSOLE COMMANDS (in addition to Q2 console commands):
-------------------------------------------------------------

+sec_attack - Secondary fire

activate - Open doors, look at or pick up things

disarm - Put away your weapon

last_message - Repeat a previous pop-up message

cure - Try and use health stuff in your inventory to best effect

use/give dagger
-bow
-crossbow
-ancestral sword
-triple flintlock
-blunderbuss
-dragon balls
-shovel
-arrows
-flaming arrows
-bolts
-studded leather
-scalemail
-ancestral armor
-rejuvenation
-health
-blur
-quickness
-bread
-bottle
-book
-torch
-water
-oil
-bile
-poison
-amaranth root
-bowl
-purified water
-cipher piece 1
-cipher piece 2
-cipher piece 3
-cipher piece 4
-vase
-screws
-piston

debug - Turn extended debugging prinouts on/off

aidebug, combatdebug, gsmdebug, miscdebug - Toggle display of specific
pieces of the debugging output

count - Toggle entity counting

log_ent - Save all entities to roarke\entities.txt

chasecam - 3pp view of yourself

===========================
WEAPONS & COMBAT
===========================

The weapon bindings are listed below:

"USE DAGGER" - Selects the dagger

"USE BOW" - Selects the bow + toggle ammo type

"USE SWORD" - Selects the sword

"USE CROSSBOW" - Selects the crossbow + toggle ammo type

"USE TRIPLE FLINTLOCK" - Selects the Flintlock Pistol

"USE BLUNDERBUSS" - Selects the blunderbuss rifle

"USE DRAGON BALLS" - Selects the Dragon Balls

"DISARM" - Deselects your current weapon

"+ATTACK" - Primary weapon attack

"+SEC_ATTACK" - Secondary weapon attack or function

Note: Most weapons have more than one attack type or can use different types of ammo. These are:

BOW - Uses regular arrows and special Fire Arrows. 

CROSSBOW - Uses regular bolts and special Explosive Bolts

SWORD - The secondary attack lets you block with the sword. JUMP + ATTACK/DOWN is a jumping chop combo that does extra damage and can knock the target down.

FLINTLOCK - The secondary attack lets you discharge all three barrels at once

RIFLE - Hold the secondary attack button down to use the scope/zoom

DAGGER - The secondary attack lets you punch for greater impact damage



WEAPONS
-------

These are the weapons available for your use in the game:

DAGGER - this was Roarke's backup weapon and is not very effective against all but the weakest enemies.

BOW - A good silent long range weapon that is useful against most enemies

SWORD - The Ancestral sword is a magical one and is more powerful than a regular sword. One of the best and most versatile weapons in the game. When you have the sword, you can use one of it's powers called Bloodlust. This will increase your speed and the amount of damage you do, similar to a constant rush of adrenaline. Another effect of Bloodlust is a much greater chance of performing a decapitation, killing the target in one hit.

CROSSBOW - More powerful at close range and faster than the bow.

TRIPLE FLINTLOCK - This is a Cheitan made sidearm. Not really a primary weapon, it is still useful at close ranges.

BLUNDERBUSS RIFLE - Another Cheitan weapon, used primarily by the Crescent Guard. Very powerful, but too slow to use at close ranges.

DRAGON BALLS - Magical explosives first created in Cheitan that deliver massive amounts of damage to anyone caught within the energy discharge that is produced.

CANNON - Sometimes you may be able to use stationary weapons such as cannons if you have the ammo for them. To use a cannon, select it, load with the SECONDARY ATTACK key and fire with the PRIMARY ATTACK key.

KNOCKDOWN - This isn't really a weapon, but an attack type. Pressing ATTACK + FWD + JUMP will let you try to knock the opponent down and do a small amount of damage. This is handy for clearing the way in front of you if several enemies have you cornered.

Note: If you are using a two-handed weapon, you will not be able to use a torch at the same time.



SWORDSMANSHIP 
-------------

The sword and dagger each have 4 moves. Primary attacks are slash, hack and stab with block as the sword's secondary attack and punch as the dagger's secondary attack. To initiate an attack, you hold down the primary attack button and move the mouse. Up lets you stab, down lets you hack, and side to side lets you slash.

The sword also has a few magical properties, one of which is Bloodlust. When in a prolonged battle, you have the ability to go into a berserk rage if armed with the sword. This increases your speed and the amount of damage you do. In Bloodlust mode, you can also sometimes decapitate enemies.



THE COMBAT SYSTEM
-----------------

Dawn of Darkness utilizes a system of damage based on weapon type and penetration. Each weapon does a certain type of damage such as impact damage for blunt weapons and cutting damage for slashing weapons or a combination of damage types. In this way, a much more realistic method for determining damage is possible because armor types can behave as they do in the real world and not as "universal" armor that protects against everything.  When fighting in Dawn Of Darkness, you should remember this and use the best weapon for the job. Enemies wearing metallic armor may not take damage from slashing attacks but suffer just as much as if wearing leather armor if you used a stabbing or very strong impact attack. Supernatural and magic using enemies may have entirely different strengths and weaknesses and might not take any damage from conventional attacks.


===============
THE ENVIRONMENT
===============

Like penetration values for armor, the level architecture also reacts realistically to attacks. Arrows and bolts will stick in wood and soft ground, but not in metal or stone. Many objects can be smashed and you can also dig using a shovel. When stuck at some point in a level, consider the options the terrain may offer, the answer to a puzzle may sometimes be in the terrain itself. 

Water will extinguish your torch temporarily and explosive or fire-based weapons will not work in water.

Remember that you can examine and objects by pressing the ACTIVATE key.

ENEMIES
-------

The most common enemies in the game are Cheitan soldiers who are invading Lucitan. They are a rag tag army consisting of imperial troops, mercenaries and Necromancers. At their aid is the magic and science of the Necromancers which allows the Cheitans to raise the recently dead, summon demons and create powerful steam powered machinery and weapons.  Some of the common troops you will encounter are:

SCOUTS
Cheitan regulars, they are usually at the forefront of an attack and are considered expendable. Scouts carry a scimitar and wooden shield and wear leather armor.
Equipment: Leather armor, wooden shield, scimitar.

OUTRIDERS
These are support troops for the Scouts, they are similar in abilities but are armed with bows. Together the Scouts and Outriders can take a toll on you, even if they seem weak at first.
Equipment: Cloth armor, bow.

SWORDSMEN
Mercenaries of many origins hired by Cheitan to replace the losses Cheitan has suffered from the previous wars.  They are all armed with swords and metal shields, but armor varies, the mercenaries will wear whatever they can get their hands on, sometimes not even wearing a Cheitan uniform at all. In general, they are better fighters than the scouts.
Equipment: Scalemail armor, reinforced shield, longsword.

AXEMEN
Another type of mercenary, similar in many ways to the swordsmen, except for carrying an axe and several throwing knives.  Axemen are slightly better fighters than swordsmen in some cases.
Equiment: Chainmail armor, battleaxe, throwing knives.

CROSSBOWMEN
Like Outriders, these soldiers tend to stay back and shoot you with their ranged weapons. If cornered they can be easily defeated, but be careful, those crossbows can deal significant damage and the bolt impacts can temporarily knock you off balance.
Equiment: Leather armor, crossbow.

OFFICERS
All Cheitan troop types are led by sergeants, lieutenants, captains and other officers the same as in any army. You can identify these by their armor, which is usually red in color or more elaborate than.  These soldiers are elite fighters and better equipped than the rest, you may also notice that the other troops will tend to protect them.
Equipment: Varies

GENERALS
Each Cheitan battalion is led by a general, an officer who has proven his skill in many battles and is fiercely loyal to Malek Taus. Generals are armed with a longsword, triangular metal shield and explosives called Dragon Balls which were developed by the Necromancers. 
Equipment: Steel platemail armor, metal shield, longsword, Dragon Balls.

NECROMANCER APPRENTICES
Members of Weirus' cult and allies of the Cheitans. They seem to have their own ulterior motives for joining Cheitan and do not usually venture out into areas where there is combat, instead they let their minions, magic and inventions do their work for them. The type encountered in Dawn Of Darkness are the Apprentices, the lower ranking class within the cult. Apprentices have two spells and can teleport.
Equipement:  Spells.

WRAITHS
The undead troops that captured Roarke. The Necromancers created them as a way to increase the size of the Cheitan army quickly, these soldiers are abnormally vicious and are armed with life-draining vampiric staves.  Wraiths need to drain life energy on a regular basis to regenerate, otherwise they will lose their physical form and cannot be revived again. When fighting Wraiths, be careful of their staves, which can shoot rays that will ensnare you and their ability to change into shadow form, becoming nearly invisible.
Equipment: Vampiric staff.

DEMONS
There are several types of demons the Necromancers have managed to coerce into following them. Hordes of lower ranking semi-intelligent demons have been summoned to fill in the remaining positions within the Cheitan army and some more powerful demons who possess human-like thought processes have been given their own authority to command troops and workers.
Equipment: Varies

OTHERS
From time to time you may encounter Cheitan workers who are unarmed, Cannoneers, warhounds and some other Cheitans.  Not all are a serious threat, but even a lowly unarmed worker may escape and bring a screaming horde of reinforcements.



NPCS AND LOCATIONS
------------------

Dawn Of Darkness takes place in the territories in and around Elvod village, a mere fraction of Lucitanian territory. You will travel to different locations as you continue on your quest to find Sylmaril as the war rages on throughout the land. Along the way, you will meet many people who you can converse with. How you interact with them is up to you, but remember that killing them or insulting them may ruin your chances to beat the Cheitans. Many times you will need to rely on the people you meet and work together, other times an NPC might provide the clue you need to solve a mission. You may sometimes need to travel back and forth between locations to complete some of the longer quests.


=====
NOTES
=====

We originally began work on this total conversion in the hopes of selling it as an add-on for Quake 2 and to gain experience in creating basically a full length game. Alas, due to hundreds of problems, not the least of which is working virtually and completely without funding, we have been delayed for over a year now. 40 team members have come and gone along the way and several computers were broken, which we had to pay for ourselves which also added to the problems.  In order to release something within a reasonable amount of time, we have cut the levels down to half as many as we originally planned. So what you are playing now is a condensed version of our original design. Along with the levels, we will release all the source code and assets so you can make your own ROLE PLAYING GAMES USING THE QUAKE 2 ENGINE.  You'll notice we've implemented a full conversation/interaction system and a Game State Management system which allows events to be tracked between levels. It's basically a script-less system that allows almost all the functionality of scripted ones. This means even a lowly mapper can make cinematic-like RPG games, including 3pp cameras for cutscenes. This was all done without changing the Q2 .exe so you can legally distribute your creations.

We hope you enjoy what we've done and consider the limitations we've worked under. Dawn Of Darkness is a great game, but this TC is only a primitive example of what our vision for action-adventure gaming really is. If things work out as we plan, expect to see something truly stunning from Ward Six in the future, not the least of which may be a standalone version of Dawn Of Darkness using an engine specifically designed for the game.



SYSTEM REQUIREMENTS
-------------------

Dawn Of Darkness has been optimized for 3D accellerated systems. You can still play the game in software mode, but not all visual effects will function correctly.

Quake 2 version 3.20 (full install recommended)
Windows 95 or NT 4.0 with 100% compatible computer system
Pentium 133 MHz processor (450 MHz recommended)
Win 95: - 32 MB RAM Required (64 MB recommended)
Win NT 4.0 - 64 MB RAM Required
100% Sound Blaster-compatible sound card
100% OpenGL Compatible 3D video card
Joystick and mouse-supported (3-button mouse recommended)



FREQUENTLY ASKED QUESTIONS
--------------------------

Crash or lockup as the game starts This is likely caused by the config.cfg file that is in the ROARKE directory. 
Solution Delete QUAKE2\ROARKE\CONFIG.CFG, restart the game. Enter the console and type EXEC DEFAULT.CFG, and then adjust settings for your hardware. It still crashes or locks up as the game starts You may need to get the 3.20 patch for Quake II. Barring that, you may need to contact us on the messageboard located at http://www.DawnOfDarkness.com/board. 

The game reports that "version is 1, you need 3" This is because you have an older version of Quake II. 
Solution Download and install the 3.20 Quake II patch. It can be found on many gaming sites. 



TEAM
----

Design: 

Alex Kazhdan, toadeater@wardsix.com - Director, writer, 2D art
Jason McIntosh, jason@wardsix.com - Producer, writer, webmaster
Marek Rabas, mrabas@wardsix.com - Code lead
Greg Tice, driedblud@wardsix.com - 2D art, concept art
Anssi Hyytiainen, anssih@nic.fi - Maps, placement, finishing

Code:

Dobes Vandermeer, dobes@mindless.com - Code, AI
Dino Morelli, dmorelli@mindspring.com - Code

Maps:

Ben Glover, Ben.Glover@btinternet.com
Cho Yan Wong, pwong@pobox.leidenuniv.nl
Brendon Chung, blender81@hotmail.com
Pete Weatherby, shadowdane@planetquake.com
Jerk Gustafsson, gish@starbreeze.com


Art:

Mark Cuthbert, amnesia@ihug.co.nz - 3D art
Eric Spitler, eric@wardsix.com - 3D art
Tom Colby, tom@wardsix.com - 3D art
Andrew Belliveau, anderin@earthlink.net - 2D art
Dan Marinelli, deranged@deathsdoor.com - 2D art

Sound:

Glenn Fricker, hellion@sprint.ca - Sound

Quality Assurance:

Josh McLean, maven@rlink.com
 
Special thanks and greetings to the following people who contributed in one way or another: Christian Salyer, Dave McLean, Jonathon Moore, Jesper Myrfors, Luke Whiteside, Bobby Pavlock, Ty Smith, Ben Caston.




CONTACT INFO
---------------------------------

If you need to get in touch with us for some reason you can email us at:

toadeater@wardsix.com or jason@wardsix.com 

If you are looking for hints or tech help, please do not email us, try the Dawn Of Darkness messageboard instead at:

http://www.dawnofdarkness.com/board/

Any patches or additions will be found at:

http://www.dawnofdarkness.com/release/
