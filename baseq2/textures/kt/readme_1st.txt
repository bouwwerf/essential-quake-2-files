-------------------------------------------------------------------------

TITLE	: assorted rock textures
AUTHOR	: ken 'kat' beyer
EMAIL	: cpdt@telinco.co.uk
URL	: http://www.quake3bits.co.uk
N� OF	: 12
SHADER	: n/a

------------------
* TEXTURE NAMES *
all files are *.tga format 256x512 unless otherwise stated

kt_512_rough1-2
kt_512_rough1b_grn2
kt_512_rough1c2
kt_512_rough1c2a
kt_512_rough1d2
kt_512_rough2-2
kt_512_rough2b
kt_512_rough2e
kt_512_rough2f
kt_512_rough2g
kt_512_rough2i2b
kt_512_rough2j
------------------
INFO	: focused on two basic textures which are used to create slightly different variations in colour and style

CREDITS
ID software, QkennyQ, EMSIPE, Eskimo Roll
DISTRIBUTION
as long as this readme is included...!

--------------------------------------------------------------------------