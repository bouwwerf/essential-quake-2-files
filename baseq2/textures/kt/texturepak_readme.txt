*information*

Rock Texture pak.
--------------------------------------------------------------------------------------
Textures are all *.tga format and a mix of alpha channeled and non-alphachanneled. 
(note: I haven't writen any scripts for these so you can create your own effects)

author		: ken 'kat' beyer
contact		: cpdt@telinco.co.uk
url		: http://www.quake3bits.co.uk

permissions	: If you can't find the texture you want feel free to use, edit and or change the textures in these paks to suite you ends. 
		: All I just ask in return is a little credit in the readme of your project 
		: but more important, 
		: I'm genuinly interested in seeing what and how you use the textures..! :o)
		: Feel free to distribute the contents by any means necessary (I want to take over the world muhahahahaaaaaaa...!)

--------------------------------------------------------------------------------------

credits		: id (pronounced 'id' not 'I.D' I recently found out..!) for Quake3Arena
thanks		: special thnx to Nigel 'P_HuKu' Cooney, Chris 'QkennyQ' Niell and Levi 'Eskimo_Roll' Webb.