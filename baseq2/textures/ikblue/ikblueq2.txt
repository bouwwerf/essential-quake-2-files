================================================================
Title                   : IKblue texture pack for Quake2 levels
Date                    : 5/28/1998
Filename                : ikblueq2.zip ,  ikblue/*.wal
Author                  : Iikka "Fingers" Keranen
                          (use &auml; instead of a in "Keranen" in html..)
Email Address           : ikeranen@mail.student.oulu.fi
Home Page               : www.planetquake.com/ikq/ (visit there!)
Snail Mail Address      : Iikka keranen
(send me your money!-)    2200 Ross avenue #5400
                          Dallas TX 75201
                          USA
Description             : These are my blue textures converted to Quake2.

Additional Credits to   : Who ever wrote Deluxe Paint on 80's. It's 
                          still the best paint program for 256-colour
                          modes.

================================================================

* Information *

This is a set of textures that can be used in add-on maps for Quake2. These 
textures are required on your hard disk if you play a level that uses these 
textures. 

Place the .wal files in quake2/baseq2/textures/ikblue/ . If they are in any other
directory, they won't work with my possibly upcoming maps...

* Construction *

Base                    : from scratch
Editor(s) used          : Deluxe Paint (the oldest version, 1985 or so..)
                          Imap (my own program)
                          Image Alchemy shareware v1.9
                          Pcx2wal (my own)
Known Bugs              : Bugs in textures? Hmm.. I don't think so. (Though not all these
                          textures have been used/tested yet..)
Build Time              : 2 minutes to convert from ikblue.

* Other Info *

Get IKSPQ2-4 levels for Quake1 to see these textures in action. Available
at http://www.planetquake.com/ikq/levels.htm .

* Legal Stuff *

These textures may be freely used and distributed, under the following
conditions:

- My name (Iikka Keranen) is mentioned. With single levels, the "Additional
  credits" section is where you put my name. With conversions and level
  paks, or projects involving several people, I would be in "authors".

- No money is charged for distributing this texture wad or ANY PRODUCT USING
  THESE TEXTURES. I am the only person who has the right to get money for
  selling these textures. You use them for free, you distribute them free!

- These textures are not modified without my permission; Modified textures
  remain my property. 

- This text file (ikblueq2.txt) is included in the product (.zip file or other)
  and is mentioned in the product's own text files.

- Commercial distribution is prohibited. Or ask me, $10k could be enough to
  change my opinion. :)

*** End of text file ***
